<?php

namespace Database\Factories;

use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TokenFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Token::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'token' => 'JjlLJ1wrlR43Lwk9asDwCMpWtsrebKgsMPhYwRH3VYNdAMLguVlM3bEenFoIi7VQ',
            'valid_to' => Carbon::now()->addDays(2)
        ];
    }
}
