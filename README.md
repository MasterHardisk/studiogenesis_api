# StudioGenesis API

### Instalación
Para la instalacion se requiere ejecutar un par de comandos

configurar el archivo .env

Aqui se deben cambiar las credenciales de la base de datos para que las migraciones funcionen.
~~~~ sh
cp .env.example -> .env
~~~~

generar el app key
~~~~ sh
php artisan key:generate
~~~~


crear un link simbolico de la carpeta storage en public para dar acceso a las imagenes
~~~~ sh
php artisan storage:link
~~~~

Instalar las depencias y paquetes para que la api funcione correctamente (se requiere tener instalado composer)
~~~~ sh
composer install
~~~~

Ejecturar las migraciones y el seed.
~~~~ sh
php artisan migrate --seed
~~~~

Se creara automaticamente un usuario y un token para poder hacer funcionar el workspace de Postman.

usuario: user@example.com

pass: 123456



### Flujo petición HTTP
1. Al hacer login se genera automaticamente un token para el usuario.
2. Se accede a la request, allí solo realiza las validaciones de tipos y de requeridos con un validador.
3. La función del controlador recibe el request y los parametros de URL si tiene.
4. Se ejecuta la funcion y se retorna un objeto con los campos necesarios para mostrar por la app.



#### License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
