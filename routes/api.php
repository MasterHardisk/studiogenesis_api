<?php

use App\Http\Controllers\LogController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\UserController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


UserController::routes();
LogController::routes();
TicketController::routes();



