<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;



class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'b_day',
        'profile',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @param string|null $token
     * @return User|null
     */
    static function findByToken($token = null) {
        if(!$token) return null;
        $user = User::join('tokens', 'tokens.user_id', 'users.id')
            ->where('tokens.token', $token)
            ->select('users.*')
            ->first();
        return $user instanceof User ? $user : null;
    }

}
