<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImagesTicket extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = "images_ticket";

    /**
     * @var string[]
     */
    protected $fillable = [
        'ticket_id',
        'path'
    ];
}
