<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use App\Models\Token;
use Closure;
use Illuminate\Http\Request;

class CheckTokenValidation extends ApiController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->header('Authorization'))return $this->respondNotAuthorization('Token is required');
        $token = Token::where('token', $request->header('Authorization'))->first();

        if(!$token || $token->valid_to <= now()->toDateTimeString()) return $this->respondNotAuthorization('Token is out to date');

        return $next($request);
    }
}
