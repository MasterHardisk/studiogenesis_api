<?php

namespace App\Http\Requests\UserRequest;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = User::findByToken($this->header('Authorization'));
        return !is_null($user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'password' => 'confirmed|required'
        ];
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'required'=> 'The :attribute field is required.',
            'confirmed' => 'passwords does not match'
        ];
    }
}
