<?php

namespace App\Http\Requests\TicketRequest;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class TicketUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = User::findByToken($this->header('Authorization'));
        return !is_null($user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'event_date' => 'required|date_format:d-m-Y H:i',
            'price' => 'required|integer'
        ];
    }


    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'required' => 'The :attribute field is required.',
            'date_format' => 'The entered date format is invalid'
        ];
    }
}
