<?php


namespace App\Http\Responses;


use App\Models\Token;
use App\Models\User;

class LoginResponse extends BaseResponse
{
    public string $token;

    /**
     * LoginResponse constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $token = Token::where('user_id', $user->id)->orderBy('created_at' ,'Desc')->first();

        $this->token = $token->token;
    }


}
