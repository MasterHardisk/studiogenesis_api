<?php


namespace App\Http\Responses;


use App\Models\User;
use Carbon\Carbon;


class UserResponse extends BaseResponse
{
    public int $id;
    public string $name;
    public string $surname;
    public ?string $image_profile;
    public string $email;
    public ?string $b_day;

    /**
     * UserResponse constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->id = $user->id;
        $this->name = $user->name;
        $this->surname = $user->surname;
        $this->image_profile = $user->profile;
        $this->email = $user->email;
        $this->b_day = Carbon::parse($user->b_day)->format('d-m-Y');
    }


}
