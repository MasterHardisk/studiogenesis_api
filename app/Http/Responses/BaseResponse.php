<?php


namespace App\Http\Responses;


use Illuminate\Support\Collection;

class BaseResponse
{

    /**
     * @param Collection $models
     * @return array
     */
    static function arrayCast(iterable $models): array
    {
        $result = [];
        $models_count = count($models);
        for ($i=0;$i<$models_count;$i++) {
            $result[] = new static($models[$i]);
        }
        return $result;
    }

    /**
     * @return false|string
     */
    public function __toString()
    {
        return json_encode($this);
    }

}
