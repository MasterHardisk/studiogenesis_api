<?php


namespace App\Http\Responses;


use App\Models\ImagesTicket;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class TicketResponse extends BaseResponse
{
    public int $id;
    public string $name;
    public string $description;
    public string $price;
    public ?string $event_date;
    public ?Collection $images_ticket;

    /**
     * TicketResponse constructor.
     * @param Ticket $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->id = $ticket->id;
        $this->name = $ticket->name;
        $this->description = $ticket->description;
        $this->price = $ticket->price;
        $this->event_date = Carbon::parse($ticket->event_date)->format('d-m-Y H:i');
        $this->images_ticket = ImagesTicket::where('ticket_id', $ticket->id)->get();
    }


}
