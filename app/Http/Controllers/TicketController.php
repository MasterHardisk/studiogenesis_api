<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckTokenValidation;
use App\Http\Requests\TicketRequest\TicketDeleteRequest;
use App\Http\Requests\TicketRequest\TicketIndexRequest;
use App\Http\Requests\TicketRequest\TicketShowRequest;
use App\Http\Requests\TicketRequest\TicketStoreRequest;
use App\Http\Requests\TicketRequest\TicketUpdateRequest;
use App\Http\Responses\TicketResponse;
use App\Models\ImagesTicket;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;


class TicketController extends ApiController
{
    /**
     * @param TicketIndexRequest $request
     * @return JsonResponse
     */
    public function index(TicketIndexRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));
        $tickets = Ticket::where('user_id', $user->id)->orderBy('event_date', 'ASC')->get();
        return $this->respond(['data' => TicketResponse::arrayCast($tickets)]);
    }

    /**
     * @param TicketShowRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function show(TicketShowRequest $request, $id): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        $ticket = Ticket::where([
            ['user_id', $user->id],
            ['id', $id]
        ])->first();

        if(!$ticket) return $this->respondNotFound('ticket not found');
        return $this->respond(['data' => new TicketResponse($ticket)]);
    }

    /**
     * @param TicketUpdateRequest $request
     */
    public function update(TicketUpdateRequest $request, $id)
    {
        $user = User::findByToken($request->header('Authorization'));
        $event_date = Carbon::parse($request->event_date)->format('Y-m-d H:i:s');

        $ticket = Ticket::where('id', $id)->first();
        if(!$ticket) return $this->respondNotFound("Ticket not found");

        $deleted = ImagesTicket::where('ticket_id', $id)->delete();
        File::deleteDirectory(public_path('storage/tickets/' . $id));


        $ticket->update([
            'name' => $request->name,
            'description' => $request->description,
            'event_date' => $event_date,
            'user_id' => $user->id,
            'price' => $request->price
        ]);



        for($i = 0; $i < 4; $i++){
            if($request->hasFile('image' . $i)){

                Storage::disk('public')->putFileAs('tickets/' . $ticket->id . '/' .  $user->id, $request->file('image' . $i), str_replace(' ', '_', $request->file('image' . $i)->getClientOriginalName()));

                ImagesTicket::create([
                    'ticket_id' => $ticket->id,
                    'path' => '/storage/tickets/' . $ticket->id . '/' . $user->id . '/' . str_replace(' ', '_', $request->file('image' . $i)->getClientOriginalName())
                ]);
            }
        }

        return $this->respond(['data' => new TicketResponse($ticket)]);
    }

    /**
     * @param TicketStoreRequest $request
     * @return JsonResponse
     */
    public function store(TicketStoreRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));
        $event_date = Carbon::parse($request->event_date)->format('Y-m-d H:i:s');

        $ticket = Ticket::create([
            'name' => $request->name,
            'description' => $request->description,
            'event_date' => $event_date,
            'user_id' => $user->id,
            'price' => $request->price
        ]);

        for($i = 0; $i < 4; $i++){
            if($request->hasFile('image' . $i)){

                Storage::disk('public')->putFileAs('tickets/' . $ticket->id . '/' .  $user->id, $request->file('image' . $i), str_replace(' ', '_', $request->file('image' . $i)->getClientOriginalName()));

                ImagesTicket::create([
                    'ticket_id' => $ticket->id,
                    'path' => '/storage/tickets/' . $ticket->id . '/' . $user->id . '/' . str_replace(' ', '_', $request->file('image' . $i)->getClientOriginalName())
                ]);
            }
        }


        return $this->respond(['data' => new TicketResponse($ticket)]);
    }

    /**
     * @param TicketDeleteRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function delete(TicketDeleteRequest $request, $id): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        $ticket = Ticket::where([
            ['user_id', $user->id],
            ['id', $id]
        ])->first();

        if(!$ticket) return $this->respondNotFound('ticket not found');

        ImagesTicket::where('ticket_id', $id)->delete();
        File::deleteDirectory(public_path('storage/tickets/' . $ticket->id));

        $ticket->delete();
        return $this->respond(['data' => ['result' => 'ticket deleted correctly']]);
    }


    public function search(TicketIndexRequest $request, $query){
        $user = User::findByToken($request->header('Authorization'));
        $tickets = Ticket::where('user_id', $user->id)->where('searchable', 'like', '%'.$query.'%')->orderBy('event_date', 'ASC')->get();
        return $this->respond(['data' => TicketResponse::arrayCast($tickets)]);
    }

    static function routes () {
        Route::middleware([CheckTokenValidation::class])->group(function () {
            Route::get('tickets', '\\' . self::class . '@index');
            Route::get('tickets/{id}', '\\' . self::class . '@show');
            Route::post('tickets', '\\' . self::class . '@store');
            Route::post('tickets/{id}', '\\' . self::class . '@update');
            Route::delete('tickets/{id}', '\\' . self::class . '@delete');
            Route::get('tickets/search/{query}', '\\' . self::class . '@search');
        });
    }
}
