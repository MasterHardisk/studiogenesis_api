<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckTokenValidation;
use App\Http\Requests\UserRequest\UpdateNameRequest;
use App\Http\Requests\UserRequest\UpdatePasswordRequest;
use App\Http\Requests\UserRequest\UpdateSurnameRequest;
use App\Http\Requests\UserRequest\UpdateImageProfileRequest;
use App\Http\Requests\UserRequest\UpdateEmailRequest;
use App\Http\Requests\UserRequest\UpdateBDayRequest;
use App\Http\Requests\UserRequest\UserShowRequest;
use App\Http\Requests\UserRequest\UserStoreRequest;
use App\Http\Responses\UserResponse;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;


class UserController extends ApiController
{
    /**
     * @param UserShowRequest $request
     * @return JsonResponse
     *
     */
    public function show(UserShowRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));
        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UserStoreRequest $request
     * @return JsonResponse
     */
    public function store(UserStoreRequest $request): JsonResponse
    {
        $password = rand(111111,999999);
        $user = User::create([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'password' => Hash::make($password),
        ]);

        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdateNameRequest $request
     * @return JsonResponse
     */
    public function updateName(UpdateNameRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));
        $user->update([
            'name' => $request->name,
        ]);

        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdateSurnameRequest $request
     * @return JsonResponse
     */
    public function updateSurname(UpdateSurnameRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        $user->update([
            'surname' => $request->surname,
        ]);

        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdateEmailRequest $request
     * @return JsonResponse
     */
    public function updateEmail(UpdateEmailRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        $user->update([
            'email' => $request->email,
        ]);

        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdateBDayRequest $request
     * @return JsonResponse
     */
    public function updateBDay(UpdateBDayRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        $new_bday = Carbon::parse($request->b_day)->toDateString();

        $user->update([
            'b_day' => $new_bday,
        ]);

        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdateImageProfileRequest $request
     * @return JsonResponse
     */
    public function updateImageProfile(UpdateImageProfileRequest $request): JsonResponse
    {
        $user = User::findByToken($request->header('Authorization'));

        if($request->hasFile('profile')){
            $target = explode('/',$user->profile);
            $target = $target[sizeof($target)-1];

            Storage::disk('public')->delete('avatars/' . $user->id . '/' . $target);

            Storage::disk('public')->putFileAs('avatars/' . $user->id, $request->file('profile'), str_replace(' ', '_', $request->file('profile')->getClientOriginalName()));
            $user->profile = '/storage/avatars/' . $user->id . '/' . str_replace(' ', '_', $request->file('profile')->getClientOriginalName());
        }else{
            $user->profile = null;
        }
        $user->save();
        return $this->respond(['data' => new UserResponse($user)]);
    }

    /**
     * @param UpdatePasswordRequest $request
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::findByToken($request->header('Authorization'));

        if(Hash::check($request->current_password, $user->password))
        {
            $user->password = Hash::make($request->password);
            $user->save();
        }else
        {
            return $this->respondNotAcceptable('current password incorrect');
        }

        return $this->respond(['data' => ['result' => 'password updated correctly']]);
    }


    static function routes () {
        Route::middleware([CheckTokenValidation::class])->group(function () {
            Route::get('users', '\\' . self::class . '@show');
            Route::post('users', '\\' . self::class . '@store');
            Route::post('users/update/image_profile', '\\' . self::class . '@updateImageProfile');
            Route::put('users/update/name', '\\' . self::class . '@updateName');
            Route::put('users/update/surname', '\\' . self::class . '@updateSurname');
            Route::put('users/update/email', '\\' . self::class . '@updateEmail');
            Route::put('users/update/b_day', '\\' . self::class . '@updateBDay');
            Route::put('users/update/password', '\\' . self::class . '@updatePassword');
        });
    }
}
