<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckTokenValidation;
use App\Http\Requests\LoginRequest;
use App\Http\Responses\LoginResponse;
use App\Models\Token;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Carbon\Carbon;

class LogController extends ApiController
{
    public function login(LoginRequest $request){

        $user = User::where('email', $request->email)->first();

        if(!$user)return $this->respondNotFound('The email entered does not appear in our database');
        if(Hash::check($request->password, $user->password))
        {

            Token::where('user_id', $user->id)->delete();

            Token::create([
                'user_id' => $user->id,
                'token' => Str::random(64),
                'valid_to' => Carbon::now()->addDays(2)
            ]);

            return $this->respond(['data' => new LoginResponse($user)]);
        }
        else return $this->respondNotAuthorization('The session could not be started because the password entered is incorrect');

    }

    public function logout(){

    }


    static function routes () {
        Route::middleware([CheckTokenValidation::class])->group(function () {
            Route::post('login', '\\' . self::class . '@login')->withoutMiddleware(CheckTokenValidation::class);
            Route::post('logout', '\\' . self::class . '@logout');
        });
    }
}
